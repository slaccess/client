package ru.slruslan.slaccess;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.loopj.android.http.*;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    public static final String PREFS_NAME = "SlAccessPrefs";
    public static final String LOG_TAG = "SLACCESS";

    Button registerBtn;
    EditText codeInput;
    SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        settings = getSharedPreferences(PREFS_NAME, 0);

        if(settings.getString("userId", null) != null) {
            Log.d(LOG_TAG, "User id is: "+settings.getString("userId", null));
            Log.d(LOG_TAG, "User name is: "+settings.getString("userName", null));

            Intent intent = new Intent(this, ScrollingActivity.class);
            startActivity(intent);
        }


        registerBtn = (Button)findViewById(R.id.button);
        registerBtn.setOnClickListener(registerBtnClick);

        codeInput = (EditText)findViewById(R.id.editText);
    }

    View.OnClickListener registerBtnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String code = codeInput.getText().toString();

            if(code.equals(""))
                showEmptyDialog();
            else {
                RequestParams data = new RequestParams();
                data.add("code", code);
                API.post("registerApplication", data, new JsonHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject json) {
                        try {
                            SharedPreferences.Editor ed = settings.edit();
                            ed.putString("userId", json.getString("userId"));
                            ed.putString("userName", json.getString("userName"));
                            ed.apply();

                            AsyncHttpClient client = new AsyncHttpClient();
                            client.get(json.getString("avatarUrl"), new FileAsyncHttpResponseHandler(getApplicationContext()) {
                                @Override
                                public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                                    // todo: failure handler
                                }

                                @Override
                                public void onSuccess(int statusCode, Header[] headers, File file) {
                                    File f = new File(getApplicationContext().getFilesDir(), "avatar.jpg");
                                    file.renameTo(f);
                                }
                            });

                            client.get(json.getString("qrUrl"), new FileAsyncHttpResponseHandler(getApplicationContext()) {
                                @Override
                                public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                                    // todo: failure handler
                                }

                                @Override
                                public void onSuccess(int statusCode, Header[] headers, File file) {
                                    File f = new File(getApplicationContext().getFilesDir(), "qr.jpg");
                                    file.renameTo(f);
                                }
                            });

                            Intent intent = new Intent(getApplicationContext(), ScrollingActivity.class);
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        }
    };

    private void showEmptyDialog() {
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Введите код")
                .setMessage("Для регистрации необходимо ввести уникальный код, выданный классным руководителем.")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {}
                })
                .show();
    }
}
