package ru.slruslan.slaccess;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import java.io.File;

public class ScrollingActivity extends AppCompatActivity {

    SharedPreferences settings;
    ImageView avatar, qr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);

        avatar = (ImageView)findViewById(R.id.avatar);
        qr = (ImageView)findViewById(R.id.qr);

        settings = getSharedPreferences(MainActivity.PREFS_NAME, 0);

        setTitle(settings.getString("userName", "Электронный пропуск"));

        File f = new File(getApplicationContext().getFilesDir(), "avatar.jpg");
        Drawable avatarDrawable = Drawable.createFromPath(f.getAbsolutePath());
        avatar.setImageDrawable(avatarDrawable);

        f = new File(getApplicationContext().getFilesDir(), "qr.jpg");
        Drawable qrDrawable = Drawable.createFromPath(f.getAbsolutePath());
        qr.setImageDrawable(qrDrawable);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }
}
